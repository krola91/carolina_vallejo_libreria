def call(boolean abortPipeline = false) {
  // Ejecutar el escaneo de SonarQube o imprimir un mensaje de prueba
  sh 'echo "Ejecución de las pruebas de calidad de código"'
  
  // Esperar durante 5 minutos con un timeout al resultado del escaneo
  def qualityGateResult = waitForQualityGate(5)
  
  // Evaluar el resultado del Quality Gate de SonarQube y abortar el pipeline si es necesario
  if (abortPipeline && qualityGateResult.status != 'OK') {
    error 'El Quality Gate de SonarQube no pasó. Se abortará el pipeline.'
  }
}


